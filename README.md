## API REST

This API REST project includes:
- CRUD for User Management
- CRUD for Product Management

## Installation

- git clone git@gitlab.com:rjfortis/elaniin-rfortis.git
- cd elaniin-rfortis
- composer install
- create database (Mysql/PostgreSQL)
- cp .env.example .env
- edit .env file (change database credentials)
- php artisan key:generate
- php artisan migrate:fresh --seed
- php artisan jwt:secret
- Use postman to test the API REST (email: admin@gmail.com, password: password)

