<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index() {

        if (request()->wantsJson()) {
            $itemsPerPage = (int) request('itemsPerPage');
            $users = User::filtered();
            return response()->json(
                [
                    "success" => true,
                    "data" => $users->paginate($itemsPerPage != 'undefined' ? $itemsPerPage : 10)
                ]
            );
        }
        abort(401);

        //return User::paginate(10);
    }

    public function show(int $id) {
        $user = User::find($id);
        return response()->json($user);
    }

    public function store() {
        $validator = Validator::make(request()->input(), [
            'name' => 'required|string|max:255',
            'phone' => 'integer|nullable',
            'username' => 'required|string|max:40|unique:users',
            'birth_date' => 'date_format:Y-m-d|before:today|nullable',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 422);
        }

        $user = User::create([
            'name' => request('name'),
            'phone' => request('phone'),
            'username' => request('username'),
            'birth_date' => request('birth_date'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
        ]);

        return response()->json(compact('user'),201);
    }

    public function update(int $id) {
        $user = User::find($id);
        if (!$user) {
            return response()->json(["message" => "Usuario no encontrado"], 404);
        }
        $validator = Validator::make(request()->input(), [
            'name' => 'required|string|max:255',
            'phone' => 'integer|nullable',
            'username' => 'required|string|max:40|unique:users,username,'.$id,
            'birth_date' => 'date_format:Y-m-d|before:today|nullable',
            'email' => 'required|string|email|max:255|unique:users,email,'.$id,
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 422);
        }

        $user->fill([
            'name' => request('name'),
            'phone' => request('phone'),
            'username' => request('username'),
            'birth_date' => request('birth_date'),
            'email' => request('email'),
            'password' => bcrypt(request('password'))
        ])->save();

        return response()->json(compact('user'),201);
    }

    public function destroy(int $id) {
        $user = User::find($id);
        if (!$user) {
            return response()->json(["message" => "Usuario no encontrado"], 404);
        }
        $user->delete();
        return response()->json(["message" => "Usuario eliminado"]);
    }
}
