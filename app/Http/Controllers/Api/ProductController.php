<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function index() {

        if (request()->wantsJson()) {
            $itemsPerPage = (int) request('itemsPerPage');
            $products = Product::filtered();
            return response()->json(
                [
                    "success" => true,
                    "data" => $products->paginate($itemsPerPage != 'undefined' ? $itemsPerPage : 10)
                ]
            );
        }
        abort(401);

        //return Product::paginate(10);
    }

    public function show(int $id) {
        $product = Product::find($id);
        return response()->json($product);
    }

    public function store() {
        $validator = Validator::make(request()->input(), [
            'sku' => 'required|string|max:20|unique:products',
            'name' => 'required|string|max:100',
            'quantity' => 'required|integer',
            'price' => 'required',
            'description' => 'string|max:600',
        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 422);
        }

        $product = Product::create([
            'sku' => request('sku'),
            'name' => request('name'),
            'quantity' => request('quantity'),
            'price' => request('price'),
            'description' => request('description'),
            'picture' => request('picture')
        ]);

        return response()->json(compact('product'),201);
    }

    public function update(int $id) {
        $product = Product::find($id);
        if (!$product) {
            return response()->json(["message" => "Producto no encontrado"], 404);
        }
        $validator = Validator::make(request()->input(), [
            'sku' => 'required|string|max:20|unique:products,sku,'.$id,
            'name' => 'required|string|max:100',
            'quantity' => 'required|integer',
            'price' => 'required',
            'description' => 'string|max:600',
        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 422);
        }

        $product->fill([
            'sku' => request('sku'),
            'name' => request('name'),
            'quantity' => request('quantity'),
            'price' => request('price'),
            'description' => request('description'),
            'picture' => request('picture')
        ])->save();

        return response()->json(compact('product'),201);
    }

    public function destroy(int $id) {
        $product = Product::find($id);
        if (!$product) {
            return response()->json(["message" => "Producto no encontrado"], 404);
        }
        $product->delete();
        return response()->json(["message" => "Producto eliminado"]);
    }
}
