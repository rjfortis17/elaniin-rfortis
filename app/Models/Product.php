<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = ["id"];

    public function scopeFiltered(Builder $builder) {
        $search = request('search');
        $sortBy = "";
        $order = "";
        if (request('sortBy')) {
            $sortBy = request('sortBy')[0];
        }
        if (request('sortDesc')) {
            $order = request('sortDesc')[0] == 'true' ? 'desc' : 'asc';
        }


        $products = $builder->select(
            'id', 'sku', 'name', 'quantity', 'price', 'description'
            //'id', 'name', 'price'
        )
            ->whereNotNull("name");
        if ($search && strlen($search) > 0) {
            $products->where('name', 'LIKE', "%$search%");
        }
        switch ($sortBy) {
            case 'name':
            case 'price': {
                $products->orderBy($sortBy, $order);
            }
        }
        return $products;
    }
}
