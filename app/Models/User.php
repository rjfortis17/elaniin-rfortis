<?php

namespace App\Models;

//use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone', 'username', 'birth_date', 'email', 'password',
    ];

    protected $guarded = ["id"];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'rj' => 'fortis'
        ];
    }

    public function scopeFiltered(Builder $builder) {
        $search = request('search');
        $sortBy = "";
        $order = "";
        if (request('sortBy')) {
            $sortBy = request('sortBy')[0];
        }
        if (request('sortDesc')) {
            $order = request('sortDesc')[0] == 'true' ? 'desc' : 'asc';
        }


        $products = $builder->select(
            'name', 'phone', 'username', 'birth_date', 'email'
        )
            ->whereNotNull("name");
        if ($search && strlen($search) > 0) {
            $products->where('name', 'LIKE', "%$search%");
        }
        switch ($sortBy) {
            case 'name':
            case 'price': {
                $products->orderBy($sortBy, $order);
            }
        }
        return $products;
    }

}
