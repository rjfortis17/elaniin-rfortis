<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'sku' => $faker->randomNumber(6),
        'name' => $faker->sentence,
        'quantity' => $faker->randomNumber(5),
        'price' => $faker->randomFloat(2, 5, 30),
        'description' => $faker->paragraph,
        'picture' => \Faker\Provider\Image::image(storage_path() . '/app/public/products', 600, 350, "technics", false),
        'created_at' => now()
    ];
});
